import classNames from '@/lib/utils/classNames'
import emailFormatCheck from '@/lib/utils/emailFormatCheck'
import { signUp } from 'pages/api/authentication'
import { FC, memo, useCallback, useContext, useEffect, useRef, useState } from 'react'
import { ModalContext } from '../../context/Modal'
import Button from '../Button'
import DropdownText from '../DropdownText'
import Input, { handleCheck } from '../Input'

interface IProps {
  handleSubmit: () => void
  handleError: () => void
}

const passwordMinL = 5

const fieldName = {
  username: 'Tên người dùng',
  email: 'Email',
  password: 'Mật khẩu',
  confirmPassword: 'Xác nhận mật khẩu',
}

const SignUpFormControl: FC<IProps> = ({ handleSubmit, handleError }) => {
  const [allowSubmit, setAllowSubmit] = useState(false)
  const [send, setSend] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')

  const username = useRef({ text: '', valid: false })
  const mail = useRef({ text: '', valid: false })
  const password = useRef({ text: '', valid: false })
  const confirmPassword = useRef({ text: '', valid: false })
  const input = useRef<HTMLInputElement>(null)

  const modal = useContext(ModalContext)

  const handleSend = useCallback(() => {
    setSend(true)
    setAllowSubmit(false)
    handleSubmit()
  }, [handleSubmit])

  const handleResetState = useCallback(() => {
    setAllowSubmit(
      password.current.valid &&
        confirmPassword.current.valid &&
        username.current.valid &&
        mail.current.valid
    )
    setErrorMessage('')
  }, [])

  const usernameCheck = useCallback<handleCheck>(
    (value, mode) => {
      const valueTrimmed = value.trim()

      const condition1 = valueTrimmed !== ''
      const condition2 = !valueTrimmed.includes(' ')
      const condition = condition1 && condition2

      username.current.valid = condition
      username.current.text = valueTrimmed

      if (mode === 'change' || mode === 'firstMount') handleResetState()

      return [
        {
          condition: condition1,
          errorMessage: 'Tên người dùng không được bỏ trống',
        },
        {
          condition: condition2,
          errorMessage: 'Tên người dùng không chứa khoảng trắng',
          showError: mode === 'blur',
        },
      ]
    },
    [handleResetState]
  )

  const emailCheck = useCallback<handleCheck>(
    (value, mode) => {
      const valueTrimmed = value.trim()

      const condition1 = valueTrimmed !== ''
      const condition2 = emailFormatCheck(valueTrimmed)
      const condition = condition1 && condition2

      mail.current.valid = condition
      mail.current.text = valueTrimmed

      if (mode === 'change' || mode === 'firstMount') handleResetState()

      return [
        {
          condition: condition1,
          errorMessage: 'Email không được bỏ trống',
        },
        {
          condition,
          errorMessage: 'Email không hợp lệ',
          showError: mode === 'blur',
        },
      ]
    },
    [handleResetState]
  )

  const passwordCheck = useCallback<handleCheck>(
    (value, mode) => {
      const valueTrimmed = value.trim()

      const condition1 = valueTrimmed !== ''
      const condition2 = !valueTrimmed.includes(' ')
      const condition3 = valueTrimmed.length >= passwordMinL
      const condition = condition1 && condition2 && condition3

      password.current.valid = condition
      password.current.text = valueTrimmed

      if (mode === 'change' || mode === 'firstMount') handleResetState()

      return [
        {
          condition: condition1,
          errorMessage: 'Mật khẩu không được trống',
        },
        {
          condition: condition2,
          errorMessage: 'Mật khẩu không chứa khoảng trắng',
          showError: mode === 'blur',
        },
        {
          condition: condition3,
          errorMessage: `Mật khẩu phải từ ${passwordMinL} ký tự`,
          showError: mode === 'blur',
        },
      ]
    },
    [handleResetState]
  )

  const confirmPasswordCheck = useCallback<handleCheck>(
    (value, mode) => {
      const valueTrimmed = value.trim()

      const condition1 = valueTrimmed === password.current.text
      const condition = condition1

      confirmPassword.current.valid = condition
      confirmPassword.current.text = valueTrimmed

      if (mode === 'change' || mode === 'firstMount') handleResetState()

      return [
        {
          condition: condition1,
          errorMessage: 'Mật khẩu không khớp',
          showError: mode === 'blur',
        },
      ]
    },
    [handleResetState]
  )

  useEffect(() => {
    if (!send) return
    ;(async () => {
      const data = {
        email: mail.current.text,
        password: password.current.text,
        username: username.current.text,
      }
      console.log('(FAKE)Sending data to login API...', data)

      const result = await signUp(data)
      console.log(result)
      if (result.status === false) {
        // Sign in failure
        setErrorMessage(result.message)
        setSend(false)
        setAllowSubmit(false)
        handleError()
        return
      }
      // Sign in successful
      modal.done()
    })()
  }, [send, handleError, modal])

  useEffect(() => {
    input.current.focus()
  }, [])

  return (
    <form className="my-4 mt-8 grid gap-4" onSubmit={(e) => e.preventDefault()}>
      <Input
        // value={'hi'}
        className="h-9"
        placeholder={fieldName.username}
        type="text"
        name="username"
        disabled={send}
        // handleCheck will be called when input is lose focus
        handleCheck={usernameCheck}
        // handleCheckOnChange will be called on every input
        handleCheckOnChange={usernameCheck}
        ref={input}
      />
      <Input
        // value={'test@gmail.com'}
        className="h-9"
        placeholder={fieldName.email}
        type="text"
        name="email"
        disabled={send}
        // handleCheck will be called when input is lose focus
        handleCheck={emailCheck}
        // handleCheckOnChange will be called on every input
        handleCheckOnChange={emailCheck}
      />
      <Input
        // value={'test1'}
        className="h-9"
        placeholder={fieldName.password}
        type="password"
        name="password"
        disabled={send}
        handleCheck={passwordCheck}
        handleCheckOnChange={passwordCheck}
      />
      <Input
        // value={'test1'}
        className="h-9"
        placeholder={fieldName.confirmPassword}
        type="password"
        name="confirm-password"
        disabled={send}
        handleCheck={confirmPasswordCheck}
        handleCheckOnChange={confirmPasswordCheck}
      />
      <div className="flex flex-col">
        <Button
          className={classNames(
            'h-9 transition',
            errorMessage ? 'bg-red-600' : 'disabled:bg-gray-3 disabled:text-gray-1'
          )}
          isWFull
          disabled={!allowSubmit}
          isLoading={send}
          onClick={handleSend}
          type="submit"
        >
          Đăng ký
        </Button>
        <DropdownText className="text-center text-xs" st="error">
          {errorMessage}
        </DropdownText>
      </div>
    </form>
  )
}

export default memo(SignUpFormControl)
