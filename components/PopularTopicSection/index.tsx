import { FC, memo, useCallback, useState } from 'react'
import Filter, { THandleChange, TOption, TOptions } from '../Filter'
import SectionContainer from '../SectionContainer'
import TopicCardBlogs from './TopicCardBlogs'

import PopularTopicsDecoSvg from '@/resources/svg/popularTopicsDeco.svg'
import HeadTitle from '../HeadTitle'

const filterOptions: TOptions = [
  { code: '1', value: 'Gần đây' },
  { code: '2', value: 'Tháng' },
  { code: '3', value: 'Năm' },
]

const PopularTopicSection: FC = () => {
  const [filter, setFilter] = useState<TOption['code']>(filterOptions[0]?.code ?? '')

  const handleChange = useCallback<THandleChange>((code) => {
    setFilter(code)
  }, [])

  return (
    <div className="relative overflow-hidden">
      <SectionContainer className="relative my-8">
        <div className="absolute inset-0 -z-10">
          <PopularTopicsDecoSvg className="h-full w-full scale-125 object-bottom" />
        </div>
        <div className="mb-6 flex items-center justify-between">
          <div className="flex-1"></div>
          <HeadTitle>Bài viết nổi bật</HeadTitle>
          <div className="flex flex-1 justify-end">
            <Filter options={filterOptions} handleChange={handleChange} />
          </div>
        </div>

        <TopicCardBlogs filter={filter} />
      </SectionContainer>
    </div>
  )
}

export default memo(PopularTopicSection)
