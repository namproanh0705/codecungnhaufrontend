import { ReactNode } from 'react'

export type DefaultProp = {
  className?: string
  children?: ReactNode
}
