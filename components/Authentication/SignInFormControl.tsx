import classNames from '@/lib/utils/classNames'
import emailFormatCheck from '@/lib/utils/emailFormatCheck'
import { signIn } from 'pages/api/authentication'
import { FC, memo, useCallback, useContext, useEffect, useRef, useState } from 'react'
import { ModalContext } from '../../context/Modal'
import Button from '../Button'
import DropdownText from '../DropdownText'
import Input, { handleCheck } from '../Input'

interface IProps {
  handleSubmit: () => void
  handleError: () => void
}

const passwordMinL = 5

const fieldName = {
  email: 'Email',
  password: 'Mật khẩu',
}

const SignInFormControl: FC<IProps> = ({ handleSubmit, handleError }) => {
  const [allowSubmit, setAllowSubmit] = useState(false)
  const [send, setSend] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')

  const mail = useRef({ text: '', valid: false })
  const password = useRef({ text: '', valid: false })
  const input = useRef<HTMLInputElement>(null)

  const modal = useContext(ModalContext)

  const handleSend = useCallback(() => {
    setSend(true)
    setAllowSubmit(false)
    handleSubmit()
  }, [handleSubmit])

  const handleResetState = useCallback(() => {
    setAllowSubmit(password.current.valid && mail.current.valid)
    setErrorMessage('')
  }, [])

  const emailCheck = useCallback<handleCheck>(
    (value, mode) => {
      const _value = value.trim()

      const condition1 = _value !== ''
      const condition2 = emailFormatCheck(_value)
      const condition = condition1 && condition2

      mail.current.valid = condition
      mail.current.text = _value

      if (mode === 'change' || mode === 'firstMount') handleResetState()

      return [
        {
          condition: condition1,
          errorMessage: 'Email không được bỏ trống',
        },
        {
          condition: condition2,
          errorMessage: 'Email không hợp lệ',
          showError: mode === 'blur',
        },
      ]
    },
    [handleResetState]
  )

  const passwordCheck = useCallback<handleCheck>(
    (value, mode) => {
      const _value = value.trim()

      const condition1 = _value !== ''
      const condition2 = !_value.includes(' ')
      const condition3 = _value.length >= passwordMinL
      const condition = condition1 && condition2 && condition3

      password.current.valid = condition
      password.current.text = _value

      if (mode === 'change' || mode === 'firstMount') handleResetState()

      return [
        {
          condition: condition1,
          errorMessage: 'Mật khẩu không được trống',
        },
        {
          condition: condition2,
          errorMessage: 'Mật khẩu không chứa khoảng trắng',
          showError: mode === 'blur',
        },
        {
          condition: condition3,
          errorMessage: `Mật khẩu phải từ ${passwordMinL} ký tự`,
          showError: mode === 'blur',
        },
      ]
    },
    [handleResetState]
  )

  useEffect(() => {
    if (!send) return
    ;(async () => {
      const data = {
        email: mail.current.text,
        password: password.current.text,
      }
      console.log('(FAKE)Sending data to login API...', data)

      const result = await signIn(data)
      console.log(result)
      if (result.status === false) {
        // Sign in failure
        setErrorMessage(result.message)
        setSend(false)
        setAllowSubmit(false)
        handleError()
        return
      }
      // Sign in successful
      modal.done()
    })()
  }, [send, handleError, modal])

  useEffect(() => {
    input.current.focus()
  }, [])

  return (
    <form className="my-4 mt-8 grid gap-4" onSubmit={(e) => e.preventDefault()}>
      <Input
        // value={'test@gmail.com'}
        className="h-9"
        placeholder={fieldName.email}
        type="text"
        name="email"
        disabled={send}
        // handleCheck will be called when input is lose focus
        handleCheck={emailCheck}
        // handleCheckOnChange will be called on every input
        handleCheckOnChange={emailCheck}
        ref={input}
      />
      <Input
        // value={'test11'}
        className="h-9"
        placeholder={fieldName.password}
        type="password"
        name="password"
        disabled={send}
        handleCheck={passwordCheck}
        handleCheckOnChange={passwordCheck}
      />
      <div className="flex flex-col">
        <Button
          className={classNames(
            'h-9 transition',
            errorMessage ? 'bg-red-600' : 'disabled:bg-gray-3 disabled:text-gray-1'
          )}
          isWFull
          disabled={!allowSubmit}
          isLoading={send}
          onClick={handleSend}
          type="submit"
        >
          Đăng nhập
        </Button>
        <DropdownText className="text-center text-xs" st="error">
          {errorMessage}
        </DropdownText>
      </div>
    </form>
  )
}

export default memo(SignInFormControl)
