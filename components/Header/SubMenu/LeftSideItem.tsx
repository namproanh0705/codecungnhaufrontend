import Button from '@/components/Button'
import classNames from '@/lib/utils/classNames'
import { forwardRef, ForwardRefRenderFunction, memo } from 'react'

interface IProps {
  index: number
  selected: boolean
  name: string
}

const LeftSideItem: ForwardRefRenderFunction<HTMLLIElement | null, IProps> = (
  { index, name, selected },
  ref
) => {
  return (
    <li className="h-10 flex-shrink-0" data-tabindex={index} ref={ref}>
      <Button
        isSimple
        isFull
        className={classNames(
          'px-4 text-left transition-all',
          selected ? 'bg-primary/10 text-base font-semibold text-primary' : 'text-sm text-gray-4'
        )}
      >
        {name}
      </Button>
    </li>
  )
}

export default memo(forwardRef(LeftSideItem))
