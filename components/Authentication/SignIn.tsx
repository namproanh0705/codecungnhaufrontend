import Logo from '@/resources/svg/logoColoredText.svg'
import { FC, memo, useCallback, useState } from 'react'
import Button from '../Button'
import Divider from '../Divider'
import AuthCard from './AuthCard'
import SignInFormControl from './SignInFormControl'
import SocialLogin from './SocialLogin'

const SignIn: FC = () => {
  const [allowSocial, setAllowSocial] = useState(true)

  const handleSubmit = useCallback(() => {
    setAllowSocial(false)
  }, [])
  const handleError = useCallback(() => {
    setAllowSocial(true)
  }, [])

  return (
    <AuthCard>
      <Logo className="mx-auto my-4 w-24" />
      <div className="mx-4">
        <SignInFormControl handleError={handleError} handleSubmit={handleSubmit} />
      </div>
      <Divider direction="horizontal" color="bg-gray-1" />
      <div className="mx-4 my-4 mb-9">
        <SocialLogin allowSocial={allowSocial} />
        <Button className="mt-6 w-fit px-3 text-left text-xs text-gray-4" isSimple>
          Quên mật khẩu
        </Button>
        <p className="px-3 text-xs text-gray-4">
          Chưa có tài khoản?
          <Button id="sign-up-submit-btn" isNoPadding isSimple isClickText className="ml-1">
            Đăng ký
          </Button>
        </p>
      </div>
    </AuthCard>
  )
}

export default memo(SignIn)
