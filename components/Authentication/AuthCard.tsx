import { FC, memo, ReactNode } from 'react'
import Card from '../Card'

interface IProps {
  children?: ReactNode
}

const AuthCard: FC<IProps> = ({ children }) => {
  return (
    <Card isNoShadow className="w-96">
      {children}
    </Card>
  )
}

export default memo(AuthCard)
