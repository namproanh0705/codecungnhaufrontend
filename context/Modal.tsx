import classNames from '@/lib/utils/classNames'
import { createContext, FC, memo, ReactNode, useEffect, useMemo, useRef, useState } from 'react'

interface IProps {
  children?: ReactNode
}
interface IModal {
  show: (JSX: JSX.Element) => void
  done: () => void
  hide: () => void
}

const ModalContext = createContext<IModal | null>(null)

const transitionTimer = 400

const Modal: FC<IProps> = ({ children }) => {
  const [show, setShow] = useState(false)
  // const [jsx, setJsx] = useState<JSX.Element>(<SignUp />)
  const [jsx, setJsx] = useState<JSX.Element>(null)

  const timeOutRef = useRef<NodeJS.Timeout>(null)
  // const jsx = useRef<JSX.Element>(null)
  const animateThis = useRef<HTMLHeadingElement>(null)
  const mouseDownEle = useRef<EventTarget>(null)
  const isRightMouse = useRef(false)

  const modal: IModal = useMemo(
    () => ({
      show(jsx_) {
        setShow((prevShow) => {
          if (!prevShow) {
            clearTimeout(timeOutRef.current)
            setJsx(jsx_)
            // jsx.current = jsx_

            return true
          }
          return prevShow
        })
      },
      hide() {
        setShow(false)
      },
      done() {
        setShow(false)

        timeOutRef.current = setTimeout(() => {
          setJsx(null)
          // jsx.current = null
        }, transitionTimer)
      },
    }),
    []
  )
  useEffect(() => {
    if (show) {
      // For scrollbar to keep visible via css
      document.body.classList.add('modal-open')
      return
    }
    document.body.classList.remove('modal-open')
  })

  return (
    <ModalContext.Provider value={modal}>
      <div
        className={classNames(
          'fixed inset-0 z-50 bg-dark-1/40 transition-opacity ease-out',
          show ? 'pointer-events-auto opacity-100' : 'pointer-events-none opacity-0'
        )}
        style={{ transitionDuration: `${transitionTimer}ms` }}
        id="modal"
        onMouseDown={(e) => {
          if (e.button === 0) {
            if (!isRightMouse.current) {
              // User didn't press right mouse
              mouseDownEle.current = e.target
              return
            }
            // User did press right mouse
            mouseDownEle.current = null
            isRightMouse.current = false
            return
          }

          if (e.button !== 2) return
          mouseDownEle.current = null
          isRightMouse.current = true
        }}
        onMouseUp={(e) => {
          if ((e.target as HTMLElement).id === 'modal' && e.target === mouseDownEle.current)
            modal.hide()
        }}
        role="presentation"
      >
        <div
          className={classNames(
            'pointer-events-none flex h-full items-center justify-center transition-transform ease-out',
            show ? 'translate-y-0' : 'translate-y-full'
          )}
          style={{ transitionDuration: `${transitionTimer}ms` }}
          ref={animateThis}
        >
          <div className="pointer-events-auto">{jsx}</div>
        </div>
      </div>
      {children}
    </ModalContext.Provider>
  )
}

export default memo(Modal)
export { ModalContext }
