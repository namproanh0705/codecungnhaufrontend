type TDimensionWatcher = (
  element: Element,
  cb: (width: number, height: number, index: number) => void
) => { stop: () => void }

const dimensionWatcher: TDimensionWatcher = (element, cb) => {
  let i = 0
  const resize_ob = new ResizeObserver(function (entries) {
    const rect = entries[0].contentRect

    const width = rect.width
    const height = rect.height

    cb(width, height, ++i)
  })
  resize_ob.observe(element)
  return {
    stop: () => {
      resize_ob.unobserve(element)
    },
  }
}

export default dimensionWatcher
