import { FC, memo } from 'react'
import BSingleSlider from '../BSingleSlider'
import SignIn from './SignIn'
import SignUp from './SignUp'

const Authentication: FC = () => {
  return (
    <BSingleSlider
      data={[
        { element: <SignUp />, id: 'sign-up-submit-btn' },
        { element: <SignIn />, id: 'test' },
        { element: <SignIn />, id: 'test2', direction: 'back' },
        { element: <SignIn />, id: 'sign-in-submit-btn', direction: 'back' },
      ]}
    >
      <SignIn />
    </BSingleSlider>
  )
}

export default memo(Authentication)
