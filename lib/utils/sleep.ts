const sleep = (ms: number): Promise<void> => {
  return new Promise((rs) => {
    setTimeout(rs, ms)
  })
}

export default sleep
