type TSignIn = (data: {
  email: string
  password: string
}) => Promise<{ status: boolean; message: string }>

type TSignUp = (data: { username: string; email: string; password: string }) => Promise<{
  status: boolean
  message: string
}>

const testAccount = {
  email: 'test@gmail.com',
  password: 'test1',
}

const signIn: TSignIn = async (data) => {
  // FAKE DELAY
  await new Promise((rs) => setTimeout(rs, 1000))

  if (JSON.stringify(testAccount) === JSON.stringify(data))
    return {
      status: true,
      message: 'Đăng nhập thành công',
    }

  return {
    status: false,
    message: '#Test : test@gmail.com - pass: test1',
  }
}

const signUp: TSignUp = async (data) => {
  // FAKE DELAY
  await new Promise((rs) => setTimeout(rs, 1000))

  if (data.username === 'hi')
    return {
      status: false,
      message: 'Tên người dùng đã tồn tại',
    }

  if (data.email === 'test@gmail.com')
    return {
      status: false,
      message: 'Email đã tồn tại',
    }

  return {
    status: true,
    message: 'Đăng ký thành công',
  }
}

export { signIn, signUp }
