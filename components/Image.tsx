import NextImage, { ImageProps } from 'next/image'
import { memo } from 'react'

const Image = ({ ...rest }: ImageProps) => <NextImage {...rest} />

export default memo(Image)
