import Button from '@/components/Button'
import { FC } from 'react'
import { CategoryChildren } from 'services/api-category.type'

interface IProps {
  data?: CategoryChildren[]
}

const RightSide: FC<IProps> = ({ data }) => {
  return (
    <div className="flex w-96 flex-col items-start pt-12">
      <div className="mb-2 inline-block w-full truncate px-9 font-semibold text-dark-2">
        Hướng dẫn nổi bật
      </div>
      {data?.[0] && (
        <div className="flex w-full flex-1 overflow-hidden px-9">
          <ul className="flex w-full flex-col overflow-auto">
            {data.map(({ name }, i) => (
              <li key={i} className="h-p-1 group w-full flex-shrink-0 text-sm text-dark-1">
                <Button
                  className="h-c-1 h-9 px-2 text-left transition active:scale-95"
                  isNoColor
                  isWFull
                  href={'#'}
                >
                  <p className="truncate">{name}</p>
                  <i className="ti-angle-right ml-auto pr-3 text-xs opacity-0 transition-all"></i>
                </Button>
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className="w-fit max-w-full px-9">
        <Button className="h-p-2 py-4 font-semibold text-primary" isWFull isSimple href={'#'}>
          <p className="truncate">Xem tất cả</p>
          <i className="ti-angle-right ml-4 text-sm transition-all"></i>
        </Button>
      </div>
    </div>
  )
}

export default RightSide
