import classNames from '@/lib/utils/classNames'
import RoundedArrowSVG from '@/resources/svg/roundedArrow.svg'
import { FC, memo, useState } from 'react'
import { DefaultProp } from 'types/DefaultProp'
import Button from './Button'
import DropdownText from './DropdownText'

interface IProps extends DefaultProp {
  title: string
  child: {
    title: string
    href?: string
  }[]
  id?: string
  altTitle?: string
  collapse?: boolean
  isShow?: boolean
}

const Title = ({ title }: { title: IProps['title'] }) => (
  <p className="font-medium text-dark-2" title={title}>
    {title}
  </p>
)
const AltTitle = ({ altTitle }: { altTitle: IProps['altTitle'] }) => (
  <p className="ml-auto shrink-0 text-dark-1">{altTitle}</p>
)

const SimpleTextList: FC<IProps> = ({
  isShow,
  id,
  title,
  altTitle,
  collapse,
  child,
  className,
}) => {
  const [show, setShow] = useState(isShow ?? true)

  return (
    <div id={id} className={className}>
      {collapse ? (
        <Button
          className="flex items-center gap-2 rounded-lg border border-custom-1 bg-light-1 p-4 text-left"
          isSimple
          isWFull
          isWrap
          handleClick={() => setShow(!show)}
        >
          <Title title={title} />
          {altTitle && <AltTitle altTitle={altTitle} />}
          <div
            className={classNames(
              'flex items-center fill-dark-2 transition-transform duration-300',
              !show && 'rotate-180'
            )}
          >
            <RoundedArrowSVG className="aspect-square w-6" />
          </div>
        </Button>
      ) : (
        <div className="flex items-center gap-2 rounded-lg border border-custom-1 bg-light-1 p-4 text-left">
          <Title title={title} />
          {altTitle && <AltTitle altTitle={altTitle} />}
        </div>
      )}

      <DropdownText isShow={show}>
        <ul className="divide-y divide-gray-1 overflow-hidden rounded-lg">
          {child.map(({ href, title }, i) => (
            <li key={i} className="text-dark-1 transition-colors duration-300 hover:bg-custom-2">
              {href ? (
                <Button href={href} className="text-left text-base" isFull isSimple isWrap>
                  <span className="block px-6 py-2">{title}</span>
                </Button>
              ) : (
                <div className="text-left text-base">
                  <span className="block px-6 py-2">{title}</span>
                </div>
              )}
            </li>
          ))}
        </ul>
      </DropdownText>
    </div>
  )
}

export default memo(SimpleTextList)
