import { fetcher } from '@/lib/client/request'
import classNames from '@/lib/utils/classNames'
import { FC, memo } from 'react'
import { TCourseList } from 'services/api-course.type'
import useSWR from 'swr'
import { DefaultProp } from 'types/DefaultProp'
import CourseCard from './CourseCard'

interface IProps extends DefaultProp {
  limit?: number
}
const CourseWrapper: FC = ({ children }) => (
  <div className="aspect-[14/19] h-auto w-full px-2 sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 2xl:w-1/6">
    {children}
  </div>
)

const CourseList: FC<IProps> = ({ limit, className }) => {
  const { data: _data } = useSWR<TCourseList>('/api/courses/', fetcher)

  return (
    <div className={classNames('-mx-2 flex flex-wrap justify-center gap-y-4', className)}>
      {_data?.results.slice(0, limit ?? _data.results.length).map((data, index) => (
        <CourseWrapper key={index}>
          <CourseCard {...data} />
        </CourseWrapper>
      ))}
      {typeof limit === 'number' && limit < _data?.results.length && (
        <CourseWrapper>
          <CourseCard more />
        </CourseWrapper>
      )}
    </div>
  )
}

export default memo(CourseList)
