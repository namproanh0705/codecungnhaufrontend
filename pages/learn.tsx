import CourseList from '@/components/CourseList'
import HeadTitle from '@/components/HeadTitle'
import SectionContainer from '@/components/SectionContainer'
import { FC, memo } from 'react'
import { DefaultProp } from 'types/DefaultProp'

interface IProps extends DefaultProp {}

const Learn: FC<IProps> = () => {
  return (
    <SectionContainer className="my-6">
      <HeadTitle>Tất cả khóa học</HeadTitle>
      <CourseList className="mt-6" />
    </SectionContainer>
  )
}

export default memo(Learn)
