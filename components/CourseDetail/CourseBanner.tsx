import { FC, memo, useCallback } from 'react'
import Image from '../Image'
import SectionContainer from '../SectionContainer'
import CourseTab, { ITabProps } from './CourseTab'

interface IBannerProps {
  title: string
  icon: string
  activeTab: string
  tabData: ITabProps[]
  description?: string
  onTabChange?: (type: string) => void
}

const Description = memo(({ desc }: { desc: string }) => {
  return (
    <div className="my-9">
      <p
        className="text-dark-1"
        dangerouslySetInnerHTML={{ __html: desc.split('\n').join('</br>') }}
      />
    </div>
  )
})
Description.displayName = 'Description'

const Avatar = memo(({ src, title }: { src: string; title: string }) => {
  return (
    <div
      className="flex aspect-square w-40 shrink-0 items-center justify-center overflow-hidden rounded-3xl bg-primary"
      title={title}
    >
      <div className="relative aspect-square w-2/3">
        <Image
          alt={'icon of ' + title}
          src={src}
          className="object-contain"
          unoptimized
          layout="fill"
        />
      </div>
    </div>
  )
})
Avatar.displayName = 'Avatar'

const CourseBanner: FC<IBannerProps> = ({
  title,
  icon,
  description,
  activeTab,
  tabData,
  onTabChange,
}) => {
  const handleClick = useCallback<ITabProps['handleClick']>(
    (type) => {
      onTabChange(type)
    },
    [onTabChange]
  )

  return (
    <div className="bg-primary/5">
      <SectionContainer className="py-12">
        <div className="flex items-start gap-6">
          <Avatar src={icon} title={title} />
          <div className="flex-1 overflow-hidden pt-6">
            <div className="text-4xl font-semibold text-primary">
              <p className="truncate" title={title}>
                {title}
              </p>
            </div>
            <ul className="mt-10 flex flex-wrap border-b border-gray-2">
              {tabData.map((d, i) => (
                <CourseTab key={i} {...d} active={d.type === activeTab} handleClick={handleClick} />
              ))}
            </ul>
          </div>
        </div>

        <Description desc={description} />
      </SectionContainer>
    </div>
  )
}

export default memo(CourseBanner)
export type { IBannerProps }
