import classNames from '@/lib/utils/classNames'
import { CSSProperties, FC, memo } from 'react'

interface IProps {
  direction?: 'horizontal' | 'vertical'
  color?: 'bg-gray-1' | 'bg-gray-2' | 'bg-gray-3' | 'bg-gray-4'
  style?: CSSProperties
  className?: string
}

const Divider: FC<IProps> = ({ className, color = 'bg-gray-2', direction = 'vertical', style }) => {
  return (
    <div
      className={classNames(
        direction === 'vertical' ? 'h-full w-px' : 'h-px w-full',
        color,
        className
      )}
      style={style}
    ></div>
  )
}

export default memo(Divider)
