import classNames from '@/lib/utils/classNames'
import { FC, memo, SVGProps } from 'react'
import Button from '../Button'
import Image from '../Image'

interface IProps {
  label: string
  alt: string
  src?: string
  Svg?: FC<SVGProps<SVGSVGElement>>
  disabled?: boolean
  handleClick?: () => void
}

const SocialButton: FC<IProps> = ({ Svg, label, alt, src, disabled, handleClick }) => {
  return (
    <Button
      className={classNames(
        'flex h-9 items-center justify-center bg-gray-1 px-2 py-1 text-sm text-dark-1 transition-all'
        // disabled && 'cursor-not-allowed'
      )}
      disabled={disabled}
      isNoColor
      isNoPadding
      handleClick={handleClick}
    >
      <div className="overflow-hidden">
        {src ? (
          <Image
            className={classNames('aspect-square w-6 object-contain', disabled && 'grayscale')}
            src={src}
            alt={alt}
          />
        ) : (
          Svg && (
            <Svg
              className={classNames('aspect-square w-6 object-contain', disabled && 'grayscale')}
            />
          )
        )}
      </div>
      <span className="flex-1 truncate pl-2">{label}</span>
    </Button>
  )
}

export default memo(SocialButton)
