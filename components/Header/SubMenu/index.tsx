import Divider from '@/components/Divider'
import { FC, memo, useCallback, useState } from 'react'
import { CategoryChildren } from 'services/api-category.type'
import LeftSide from './LeftSide'
import RightSide from './RightSide'

interface IProps {
  data: CategoryChildren[]
}

const SubMenu: FC<IProps> = ({ data }) => {
  const [courseIndex, setCourseIndex] = useState(0)

  const handleClick = useCallback((index: number) => {
    setCourseIndex(() => index)
  }, [])

  return (
    <div className="shadow-1 pointer-events-none absolute top-full left-0 mt-2.5 h-96 rounded-b-2xl opacity-0 transition before:absolute before:-top-3 before:h-3 before:w-full group-hover:pointer-events-auto group-hover:opacity-100">
      <div className="flex h-full overflow-hidden rounded-b-2xl bg-white">
        <LeftSide data={data} courseIndex={courseIndex} handleClick={handleClick} />
        <Divider color="bg-gray-1" />
        <RightSide data={data[courseIndex]?.children} />
      </div>
    </div>
  )
}

export default memo(SubMenu)
