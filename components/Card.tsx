import classNames from '@/lib/utils/classNames'
import { FC, HTMLAttributes, memo } from 'react'
import { DefaultProp } from 'types/DefaultProp'

interface IProps extends HTMLAttributes<HTMLDivElement>, DefaultProp {
  isNoShadow?: boolean
}

const Card: FC<IProps> = ({ isNoShadow, className, children, ...rest }) => {
  return (
    <div
      className={classNames(
        'relative overflow-hidden rounded-2xl bg-white',
        isNoShadow ?? 'shadow-xl',
        className
      )}
      {...rest}
    >
      {children}
    </div>
  )
}

export default memo(Card)
