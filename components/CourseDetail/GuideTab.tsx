import { FC, memo, useMemo } from 'react'
import { CourseDetailGuide } from 'services/api-course-detail-guide.type'
import SectionContainer from '../SectionContainer'
import SimpleTextList from '../SimpleTextList'

interface IGuideTabProps {
  data: CourseDetailGuide[]
}

const GuideTab: FC<IGuideTabProps> = ({ data }) => {
  const noSpaceTitles = useMemo(() => data?.map(({ title }) => title.replace(/\s+/g, '_')), [data])

  return (
    <SectionContainer className="-mt-9 grid grid-cols-3 gap-8">
      {data ? (
        <>
          <div className="sticky top-14 h-fit">
            <SimpleTextList
              title="Chương"
              child={data?.map(({ title }, i) => ({ title, href: `#${noSpaceTitles[i]}` }))}
            />
          </div>
          <div className="col-span-2 grid gap-4">
            {data.map(({ title, child, alt }, i) => (
              <SimpleTextList
                key={title}
                id={noSpaceTitles[i]}
                title={title}
                altTitle={alt}
                collapse
                child={child}
              />
            ))}
          </div>
        </>
      ) : (
        <span>Không có dữ liệu</span>
      )}
    </SectionContainer>
  )
}

export default memo(GuideTab)
