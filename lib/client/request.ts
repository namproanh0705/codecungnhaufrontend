import axios from 'axios'
import { config as configBase } from 'constants/config'

export const apiClientBrowser = axios.create({
  baseURL: configBase.apiBaseUrl,
})

apiClientBrowser.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

apiClientBrowser.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    return Promise.reject(error)
  }
)

export const fetcher = async (url: string) => apiClientBrowser.get(url).then((res) => res.data)
