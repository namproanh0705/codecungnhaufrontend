import classNames from '@/lib/utils/classNames'
import { FC, memo, ReactNode, useEffect, useRef } from 'react'
import { DefaultProp } from 'types/DefaultProp'

const style = { error: 'mx-3 mt-2 text-red-600' }
interface IProps extends DefaultProp {
  children?: ReactNode
  isShow?: boolean
  className?: string
  st?: keyof typeof style
}

const DropdownText: FC<IProps> = ({ isShow, className, st, children }) => {
  const divEle = useRef<HTMLDivElement>(null)
  const lastValue = useRef<ReactNode>('')

  isShow ??= true //By default this will show, but if user already set isShow = false, it won't show

  useEffect(() => {
    const height = isShow && children ? (divEle.current.children[0] as HTMLElement).offsetHeight : 0

    divEle.current && (divEle.current.style.height = `${height}px`)

    lastValue.current = children
  }, [children, isShow])

  return (
    <div
      className={classNames(
        'overflow-hidden transition-all duration-300',
        st && style[st],
        className
      )}
      ref={divEle}
    >
      <span>{children || lastValue.current}</span>
    </div>
  )
}

export default memo(DropdownText)
