import { CategoryList } from 'services/api-category.type'

export default function modifyHeaderData(data: CategoryList): CategoryList {
  if (!data) return undefined

  const navData = data.results.reduce(
    (total, item) => (item.children.length > 0 ? [...total, item] : total),
    []
  )
  // const navData = data.results
  // navData.forEach((r) => {
  // r.children = r.children.reduce((total, item) => {
  //   return item.children.length > 0 ? [...total, item] : total
  // }, [])
  // })
  return Object.assign(data, { results: navData })
}
