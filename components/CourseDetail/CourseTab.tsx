import classNames from '@/lib/utils/classNames'
import BookSVG from '@/resources/svg/book-heart.svg'
import BoxSVG from '@/resources/svg/box.svg'
import BracketSVG from '@/resources/svg/brackets.svg'
import { FC, memo, useCallback } from 'react'
import Button from '../Button'

interface ITabProps {
  title: string
  type: string
  active?: boolean
  handleClick?: (type: string) => void
}

const icons = {
  guide: <BookSVG className="aspect-square w-6" />,
  example: <BracketSVG className="aspect-square w-6" />,
  relative: <BoxSVG className="aspect-square w-6" />,
  default: <BracketSVG className="aspect-square w-6" />,
}
const CourseTab: FC<ITabProps> = ({ title, active, type, handleClick }) => {
  const handleClick_ = useCallback(() => {
    handleClick(type)
  }, [handleClick, type])

  return (
    <li className="max-w-full overflow-hidden">
      <Button
        isSimple
        className={classNames(
          'relative flex gap-3 px-6 py-3 transition-colors duration-300 after:absolute after:inset-x-0 after:bottom-0 after:block after:h-1 after:rounded-t-xl after:bg-current after:transition-transform after:duration-300',
          active ? 'fill-primary text-primary' : 'fill-gray-4 text-gray-4 after:translate-y-1'
        )}
        title={title}
        handleClick={handleClick_}
      >
        <div>{icons[type] ?? icons['default']}</div>
        <p className="truncate text-lg">{title}</p>
      </Button>
    </li>
  )
}

export default memo(CourseTab)
export type { ITabProps }
