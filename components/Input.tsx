import classNames from '@/lib/utils/classNames'
import {
  ChangeEvent,
  forwardRef,
  ForwardRefRenderFunction,
  InputHTMLAttributes,
  memo,
  ReactNode,
  useCallback,
  useEffect,
  useState,
} from 'react'
import Button from './Button'
import DropdownText from './DropdownText'

type mode = 'change' | 'blur' | 'firstMount'

type handleCheck = (
  value: string,
  mode: mode
) =>
  | { condition: boolean; errorMessage?: string; showError?: boolean }
  | { condition: boolean; errorMessage?: string; showError?: boolean }[]
type handleChange = (value: string) => void
interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  placeholder?: string
  className?: string
  icon?: JSX.Element
  type?: 'password' | 'text'
  value?: string
  children?: ReactNode
  handleChange?: handleChange
  handleCheck?: handleCheck
  handleCheckOnChange?: handleCheck
}

const Input: ForwardRefRenderFunction<HTMLInputElement, IProps> = (
  {
    className,
    icon,
    name,
    placeholder,
    type,
    value,
    disabled,
    children,
    handleChange,
    handleCheck,
    handleCheckOnChange,
    ...rest
  },
  ref
) => {
  const [text, setText] = useState(value ?? '')
  const [showPW, setShowPW] = useState(false)
  const [firstPress, setFirstPress] = useState(!!value)
  const [errorMessage, setErrorMessage] = useState('')

  const _handleCheck = useCallback(
    (text: string, callback: handleCheck, mode: mode) => {
      if (!callback) return

      const result = callback(text, mode)

      if (Array.isArray(result)) {
        const _result = result.find((r) => !r.condition)

        !_result
          ? firstPress && setErrorMessage('')
          : firstPress &&
            (_result.showError || _result.showError === undefined) &&
            setErrorMessage(_result.errorMessage ?? '<Lỗi chưa được đặt tên>')

        return
      }

      result.condition
        ? firstPress && setErrorMessage('')
        : firstPress &&
          (result.showError || result.showError === undefined) &&
          setErrorMessage(result.errorMessage ?? '<Lỗi chưa được đặt tên>')
    },
    [firstPress]
  )

  const onChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const newText = e.target.value

      setFirstPress(true)
      setText(newText)
      firstPress && setErrorMessage('')

      handleChange && handleChange(newText)
      _handleCheck(newText, handleCheckOnChange, 'change')
    },
    [firstPress, _handleCheck, handleChange, handleCheckOnChange]
  )

  useEffect(() => {
    if (handleCheck || handleCheckOnChange)
      _handleCheck(text, handleCheck || handleCheckOnChange, 'firstMount')
  }, []) // eslint-disable-line

  return (
    <div>
      <div
        className={classNames(
          'flex max-w-full items-center rounded-lg border transition',
          disabled
            ? 'border-transparent bg-gray-1 text-gray-3'
            : errorMessage
            ? 'border-red-600 bg-red-600/10 text-red-600'
            : text
            ? 'border-transparent bg-primary/10 text-dark-2'
            : 'border-transparent bg-gray-1 text-dark-2',
          !handleCheck && !handleCheckOnChange && 'h-full',
          className
        )}
      >
        {icon && <div className="flex pl-3">{icon}</div>}
        <input
          className={classNames(
            'h-full w-full appearance-none rounded-lg border-transparent bg-transparent',
            'placeholder:text-xs placeholder:font-light placeholder:text-gray-3',
            'autofill:bg-red-400 autofill:text-red-400',
            'focus:border-transparent focus:ring-0',
            type === 'password' ? 'pr-0' : 'pr-2'
          )}
          type={showPW ? 'text' : type ?? 'text'}
          placeholder={placeholder}
          value={text}
          onChange={onChange}
          disabled={disabled}
          name={name ?? 'input'}
          {...rest}
          onBlur={() => _handleCheck(text, handleCheck, 'blur')}
          ref={ref}
        />
        {type === 'password' && (
          <Button
            className={classNames(
              'ti-eye flex h-full w-8 cursor-pointer items-center justify-center transition-all',
              disabled ? 'cursor-default' : 'hover:text-dark-2',
              showPW ? 'text-base text-current' : 'text-sm text-gray-2'
            )}
            onClick={() => {
              disabled || setShowPW(!showPW)
            }}
            onKeyDown={() => {
              console.log('Down')
            }}
            isSimple
          />
        )}
        {children}
      </div>
      <DropdownText className="text-xs" st="error">
        {errorMessage}
      </DropdownText>
      {/* <div
        className={classNames(
          'mx-3 overflow-hidden text-xs text-red-600 transition-all',
          errorMessage && 'mt-2'
        )}
        ref={errorEleContainer}
      >
        <span>{errorMessage}</span>
      </div> */}
    </div>
  )
}

export default memo(forwardRef(Input))

export type { handleCheck, handleChange }
