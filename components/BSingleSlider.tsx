import classNames from '@/lib/utils/classNames'
import dimensionWatcher from '@/lib/utils/dimesionWatcher'
import sleep from '@/lib/utils/sleep'
import { FC, useCallback, useEffect, useRef, useState } from 'react'
import Card from './Card'

interface IProps {
  data?: {
    id: string
    direction?: 'next' | 'back'
    element: JSX.Element
  }[]
  children: JSX.Element
}

const transitionDuration = 400
const delayCss = 10

const BSingleSlider: FC<IProps> = ({ data, children }) => {
  const [LeftJsx, setLeftJsx] = useState<JSX.Element>(children)
  const [RightJsx, setRightJsx] = useState<JSX.Element>(null)

  const [turn, setTurn] = useState(!1)
  const [currentSlide, setCurrentSlide] = useState<'left' | 'right'>('left')
  const [slide, setSlide] = useState(false)

  const containerEle = useRef<HTMLDivElement>(null)
  const leftEle = useRef<HTMLDivElement>(null)
  const rightEle = useRef<HTMLDivElement>(null)
  const observerLeft = useRef<ReturnType<typeof dimensionWatcher>>(null)
  const observerRight = useRef<ReturnType<typeof dimensionWatcher>>(null)

  const slideNext = (Jsx: JSX.Element) => {
    setSlide(!0)
    setCurrentSlide('right')

    setTurn(!turn)

    if (!turn) setRightJsx(Jsx)
    else setLeftJsx(Jsx)
  }

  const slideBack = (Jsx: JSX.Element) => {
    setSlide(!0)
    setCurrentSlide('left')

    setTurn(!turn)

    if (!turn) setRightJsx(Jsx)
    else setLeftJsx(Jsx)
  }

  const setDimension = useCallback(
    async (
      turn2: typeof turn,
      width?: number,
      height?: number,
      noTransition?: boolean
    ): Promise<void> => {
      if (!containerEle.current) return

      const child = turn2 ? rightEle.current : leftEle.current

      const w = Math.floor(width) ?? child.offsetWidth
      const h = height ?? child.offsetHeight

      const { style } = containerEle.current

      if (noTransition) {
        style.transitionDuration = `0ms`
      } else style.transitionDuration = `${transitionDuration}ms`

      style.width = `${w}px`
      style.height = `${h}px`
    },
    []
  )

  // Slide NEXT
  useEffect(() => {
    if (!slide || currentSlide !== 'right') return

    //When rightEle is Empty (Exam: first mount and then slideNEXT)
    if (turn) {
      ;(async () => {
        leftEle.current.style.transition = `${0}ms`
        rightEle.current.style.transition = `${0}ms`

        leftEle.current.style.transform = `translateX(0%)`
        rightEle.current.style.transform = `translateX(100%)`

        await sleep(delayCss)
        // Remove transition + slide
        leftEle.current.style.transition = `${transitionDuration}ms`
        rightEle.current.style.transition = `${transitionDuration}ms`

        leftEle.current.style.transform = `translateX(-100%)`
        rightEle.current.style.transform = `translateX(0%)`

        await sleep(transitionDuration)
        // Remove transition
        leftEle.current.style.transition = ``
        rightEle.current.style.transition = ``
      })()
      return
    }
    //When rightEle is the current JSX on screen
    ;(async () => {
      leftEle.current.style.transition = `${0}ms`
      rightEle.current.style.transition = `${0}ms`

      leftEle.current.style.transform = `translateX(100%)`
      rightEle.current.style.transform = `translateX(0%)`

      await sleep(delayCss)

      // Remove transition + slide
      leftEle.current.style.transition = `${transitionDuration}ms`
      rightEle.current.style.transition = `${transitionDuration}ms`

      leftEle.current.style.transform = `translateX(0%)`
      rightEle.current.style.transform = `translateX(-100%)`

      // Remove transition
      await sleep(transitionDuration)

      leftEle.current.style.transition = ``
      rightEle.current.style.transition = ``
    })()
  })

  // Slide BACK
  useEffect(() => {
    if (!slide || currentSlide !== 'left') return

    // console.log('slideBACK', currentSlide, slide, turn)
    //When rightEle is Empty (Exam: first mount and then slideNEXT)
    if (turn) {
      ;(async () => {
        leftEle.current.style.transition = `${0}ms`
        rightEle.current.style.transition = `${0}ms`

        leftEle.current.style.transform = `translateX(0%)`
        rightEle.current.style.transform = `translateX(-100%)`

        await sleep(delayCss)
        // Remove transition + slide
        leftEle.current.style.transition = `${transitionDuration}ms`
        rightEle.current.style.transition = `${transitionDuration}ms`

        leftEle.current.style.transform = `translateX(100%)`
        rightEle.current.style.transform = `translateX(0%)`

        await sleep(transitionDuration)
        // Remove transition
        leftEle.current.style.transition = ``
        rightEle.current.style.transition = ``
      })()
      return
    }
    //When rightEle is the current JSX on screen
    ;(async () => {
      leftEle.current.style.transition = `${0}ms`
      rightEle.current.style.transition = `${0}ms`

      leftEle.current.style.transform = `translateX(-100%)`
      rightEle.current.style.transform = `translateX(0%)`

      await sleep(delayCss)
      // Remove transition + slide
      leftEle.current.style.transition = `${transitionDuration}ms`
      rightEle.current.style.transition = `${transitionDuration}ms`

      leftEle.current.style.transform = `translateX(0%)`
      rightEle.current.style.transform = `translateX(100%)`

      await sleep(transitionDuration)
      // Remove transition
      leftEle.current.style.transition = ``
      rightEle.current.style.transition = ``
    })()
  })

  useEffect(() => {
    ;(async () => {
      if (turn) {
        // rightEle is on the screen
        observerLeft.current?.stop()

        sleep(transitionDuration)

        observerRight.current = dimensionWatcher(rightEle.current, (width, height, i) => {
          // console.log(width, height, i, 'right')

          setDimension(turn, width, height, i !== 1)
        })
        return
      }

      // leftEle is on the screen
      observerRight.current?.stop()

      sleep(transitionDuration)

      observerLeft.current = dimensionWatcher(leftEle.current, (width, height, i) => {
        // console.log(width, height, i, 'left')

        setDimension(turn, width, height, i !== 1)
      })
    })()
  }, [turn, setDimension])

  // Set width and height for smooth transition
  useEffect(() => {
    setDimension(turn)
  }, [turn, setDimension])

  return (
    <Card>
      <div
        id="BSingleSlider"
        className="bg relative"
        role={'presentation'}
        onKeyDown={(e) => {
          console.log(e)
        }}
        onClick={(e) => {
          const target = e.target as Element

          const result = data.find((r) => r.id === target.id)
          if (!result) return

          result.direction ??= 'next'

          if (result.direction === 'next') {
            slideNext(result.element)
            return
          }

          slideBack(result.element)
        }}
        ref={containerEle}
      >
        <div className="relative flex">
          <div
            className={classNames(
              'absolute top-0 left-0 h-fit transition-transform will-change-transform',
              turn && 'z-10'
            )}
            ref={leftEle}
          >
            {LeftJsx}
          </div>
          <div
            className={classNames(
              'absolute top-0 left-0 h-fit transition-transform will-change-transform',
              !turn && 'z-10'
            )}
            ref={rightEle}
          >
            {RightJsx}
          </div>
        </div>
      </div>
    </Card>
  )
}

export default BSingleSlider
