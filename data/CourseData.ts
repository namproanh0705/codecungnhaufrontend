const projectsData = [
  {
    title: `C`,
    imgSrc: '/static/images/bgHC1.svg',
    iconSrc: '/static/images/c.svg',
    href: '/blog',
  },
  {
    title: 'C++',
    imgSrc: '/static/images/bgHC2.svg',
    iconSrc: '/static/images/c++.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    imgSrc: '/static/images/bgHC3.svg',
    iconSrc: '/static/images/python.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    imgSrc: '/static/images/bgHC4.svg',
    iconSrc: '/static/images/python.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    imgSrc: '/static/images/bgHC1.svg',
    iconSrc: '/static/images/python.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    imgSrc: '/static/images/bgHC2.svg',
    iconSrc: '/static/images/python.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    imgSrc: '/static/images/bgHC3.svg',
    iconSrc: '/static/images/python.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    imgSrc: '/static/images/bgHC4.svg',
    iconSrc: '/static/images/python.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    imgSrc: '/static/images/bgHC1.svg',
    iconSrc: '/static/images/python.svg',
    href: '/blog',
  },
]

export default projectsData
