import { FC, memo } from 'react'
import Button from '../Button'
import SectionContainer from '../SectionContainer'
import { SectionSvg } from '../svg/Section'

const Section2: FC = () => {
  return (
    <SectionContainer>
      <div className=" my-5 grid w-full grid-cols-2">
        <div className="col-span-1  hidden sm:hidden lg:block xl:block">
          <div className="grid grid-cols-6">
            <div className="col-start-1 xl:col-start-2">
              <SectionSvg />
            </div>
          </div>
        </div>
        <div className="col-span-2 sm:col-span-2 md:col-span-2  lg:col-span-1">
          <div className="grid grid-cols-12">
            <div className=" col-span-10 col-start-3 xl:col-start-2">
              <div className=" flex h-full flex-col items-start justify-between">
                <div className="row-span-1">
                  <p className="mt-10 text-4xl font-semibold">Cộng đồng chia sẻ kiến thức</p>
                </div>
                <div className="row-span-1">
                  <div className="text-lg font-medium">
                    <p>
                      Các bài viết theo chủ đề: {''}
                      <Button className="rounded-full bg-yellow-400 p-1 text-white">
                        Cập nhật công nghệ
                      </Button>
                    </p>
                  </div>
                  <div className="text-lg font-medium">
                    <Button className="mt-1 rounded-full bg-blue-500 p-1 text-white">
                      Chia sẻ kinh nghiệm
                    </Button>
                    <span> </span>
                    <Button className="rounded-full bg-blue-500 px-4 py-1 text-white"> ...</Button>
                  </div>
                </div>
                <div className="row-span-1">
                  <div className="flex h-32 items-end">
                    <Button className="m-5 mb-10 h-14 w-4/6 rounded-xl bg-blue-800 p-4 text-lg text-white  hover:scale-95 hover:duration-200">
                      Đăng kí ngay
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </SectionContainer>
  )
}

export default memo(Section2)
