import { fetcher } from '@/lib/client/request'
import modifyHeaderData from '@/lib/utils/modifyHeaderData'
import Logo from '@/resources/svg/logoColoredText.svg'
import { FC, memo, useContext } from 'react'
import { CategoryList } from 'services/api-category.type'
import useSWR from 'swr'
import { ModalContext } from '../../context/Modal'
import Authentication from '../Authentication'
import Button from '../Button'
import Divider from '../Divider'
import Input from '../Input'
import ThemeSwitch from '../ThemeSwitch'
import SubMenu from './SubMenu'

const Header: FC = () => {
  const modal = useContext(ModalContext)
  // const r = useRef<HTMLButtonElement>(null)

  const { data: _navData } = useSWR<CategoryList>('/api/category/', fetcher)
  const navData: CategoryList = modifyHeaderData(_navData)

  return (
    <>
      <header>
        <div className="fixed top-0 left-0 z-10 flex h-14 w-screen items-center justify-between bg-white px-12">
          <div className="flex h-9 gap-4">
            <Button className="my-auto" isWrap isHFull isSimple href="/">
              <Logo className="h-12" />
            </Button>
            <Divider />
            <ul className="flex">
              {navData?.results.map(({ id, name, children }) => (
                <li key={id} className="group relative flex h-full items-center">
                  <Button
                    className="px-4 text-primary-1 group-hover:font-medium group-hover:text-primary md:transition-all"
                    isHFull
                    isSimple
                    href={'#'}
                  >
                    <p>
                      {name}
                      {children.length > 0 && (
                        <i className="ti-angle-right ml-2 inline-block text-xs group-hover:rotate-90 md:transition"></i>
                      )}
                    </p>
                  </Button>
                  {children.length > 0 && <SubMenu data={children} />}
                </li>
              ))}
            </ul>
          </div>
          <div className="flex h-9 gap-4">
            <Input
              placeholder="Tìm kiếm"
              name="search"
              icon={<i className="ti-search text-xl text-gray-3"></i>}
              className="w-11 transition-all focus-within:w-56 hover:w-56"
            />
            <ThemeSwitch />
            <Divider />
            <Button
              onClick={() => {
                modal.show(<Authentication />)
              }}
              // ref={r}
            >
              Đăng nhập
            </Button>
          </div>
          {/* MASK SHADOW */}
          <div className="shadow-1 pointer-events-none absolute inset-0 z-10"></div>
        </div>
        <div className="pt-14"></div>
      </header>
    </>
  )
}

export default memo(Header)
