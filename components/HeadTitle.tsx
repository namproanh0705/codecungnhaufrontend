import { FC, memo, ReactNode } from 'react'

interface IProps {
  children?: ReactNode
}

const HeadTitle: FC<IProps> = ({ children }) => {
  return <h1 className="text-center text-4xl font-semibold text-dark-2">{children}</h1>
}

export default memo(HeadTitle)
