import classNames from '@/lib/utils/classNames'
import { CSSProperties, memo } from 'react'
import { DefaultProp } from 'types/DefaultProp'

interface Props extends DefaultProp {
  style?: CSSProperties
}

export default memo(function SectionContainer({ className, style, children }: Props) {
  return (
    <div className={classNames('container mx-auto px-12', className)} style={style}>
      {children}
    </div>
  )
})
