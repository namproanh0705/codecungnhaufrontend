import { FC, memo, ReactNode, useRef } from 'react'
import { CategoryChildren } from 'services/api-category.type'
import LeftSideItem from './LeftSideItem'

interface IProps {
  children?: ReactNode
  data: CategoryChildren[]
  courseIndex: number
  handleClick?: (index: number) => void
}

// const transitionTimer = 200

const LeftSide: FC<IProps> = ({ data, courseIndex, handleClick }) => {
  // const currentCourse = useRef<HTMLLIElement>(null)
  const subHeaderLeftSide = useRef<HTMLUListElement>(null)
  // const floatingFlag = useRef<HTMLDivElement>(null)

  // const timeOutRef = useRef<NodeJS.Timeout>(null)

  // Floating flag is a mask outside of the ul, so every time ul is scrolling, it need to update it position by setting transform and translateY
  // const onScroll = () => {
  //   // console.log({
  //   //   subHeaderLeftSide: subHeaderLeftSide.current,
  //   //   currentCourse: currentCourse.current,
  //   // })
  //   // console.log({
  //   //   subHeaderLeftSide: subHeaderLeftSide.current.scrollTop,
  //   //   currentCourse: currentCourse.current.offsetTop,
  //   // })

  //   floatingFlag.current.style.transform = `translateY(${
  //     -subHeaderLeftSide.current.scrollTop + courseIndex * 40
  //     // -subHeaderLeftSide.current.scrollTop + (currentCourse.current?.offsetTop || 0)
  //   }px)`
  // }

  // When courseIndex get updated, update floating flag position
  // useEffect(() => {
  //   clearTimeout(timeOutRef.current)

  //   floatingFlag.current.style.transition = `${transitionTimer}ms`
  //   onScroll()

  //   timeOutRef.current = setTimeout(() => {
  //     floatingFlag.current.style.transition = ''
  //   }, transitionTimer)
  // })

  // Prevent this floating flag from being re-render
  // const floatingFlagJsx = useMemo(
  //   () => (
  //     <div
  //       className="pointer-events-none absolute left-0 -right-2 -z-10 h-10 origin-left rounded-r-lg bg-primary/10 will-change-transform"
  //       ref={floatingFlag}
  //     ></div>
  //   ),
  //   []
  // )
  return (
    <>
      {/* <div className="relative z-10 flex w-36 overflow-visible"> */}
      {/* <div className="relative z-10 flex w-32 overflow-visible"> */}
      {/* {floatingFlagJsx} */}
      <ul
        className="hideScrollbar relative z-10 flex w-36 flex-col overflow-auto"
        ref={subHeaderLeftSide}
        // onScroll={onScroll}
        onMouseMove={(e) => {
          const ele = (e.target as Element).closest('li[data-tabindex]')
          ele && handleClick(parseInt(ele.getAttribute('data-tabindex')))
        }}
      >
        {data.map((r, i) => (
          <LeftSideItem
            key={i}
            index={i}
            selected={courseIndex === i}
            // ref={currentCourse}
            name={r.name}
          />
        ))}
      </ul>
      {/* </div> */}
    </>
  )
}

export default memo(LeftSide)
