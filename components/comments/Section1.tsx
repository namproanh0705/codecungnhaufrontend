import { FC, memo } from 'react'
import Button from '../Button'
import SectionContainer from '../SectionContainer'
import { SvgSection } from '../svg/SvgSection2'

const Section1: FC = () => {
  return (
    <SectionContainer>
      <div className="my-5 w-full">
        {/* <>{bg}</> */}
        <div className="grid grid-cols-12">
          <div className="col-span-12 sm:col-span-4 sm:col-start-2 ">
            <div className="">
              <div className="text-blue-900">
                <p className="text-5xl">Học</p>
                <p className="text-6xl font-bold">Lập Trình</p>
              </div>
              <div className="my-7 text-blue-900">
                <p>
                  Tổng hợp kiến thức, thân thiên với người mới bắt đầu và có vô vàn bài luyện tập để
                  luyện tay
                </p>
              </div>
              <div>
                <Button
                  type="button"
                  className="w-full rounded-xl bg-blue-800 py-3 font-semibold text-white hover:scale-95 hover:bg-blue-700  hover:duration-200 sm:px-20"
                >
                  Học ngay
                </Button>
              </div>
            </div>
          </div>
          <div className=" hidden sm:block">
            <SvgSection />
          </div>
        </div>
      </div>
    </SectionContainer>
  )
}

export default memo(Section1)
