import { useState } from 'react'
import SectionContainer from '../SectionContainer'

export const Subscribe = () => {
  const [state, setState] = useState({
    email: '',
  })
  const [display, setDisplay] = useState(false)

  const handleChange = (e) => {
    const { name, value } = e.target
    setState({ ...state, [name]: value })
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    console.log(state)
  }
  const displayCss = display ? 'hidden' : ''
  const buttonCss = display ? 'rounded-lg w-full duration-200' : ''

  return (
    <SectionContainer>
      <div className="flex items-center justify-center">
        <div className="p-5">
          <h1 className="text-center text-4xl font-semibold text-blue-900">Đăng kí nhận tin</h1>
          <form
            style={{ width: 450 }}
            onSubmit={handleSubmit}
            className="flex w-full items-center justify-center p-5"
          >
            <input
              className={`rounded-tl-lg rounded-bl-lg bg-gray-200 p-2 px-7 text-gray-600 ${displayCss}`}
              placeholder="Email..."
              name="email"
              onChange={handleChange}
            />
            <button
              onClick={() => {
                setDisplay(!display)
              }}
              type="submit"
              className={`rounded-tr-lg rounded-br-lg bg-blue-800 p-2  px-7 text-white hover:scale-95 hover:bg-blue-700 hover:duration-200 ${buttonCss}`}
            >
              Xác nhận
            </button>
          </form>
          <p className="pb-5 text-center text-sm text-gray-400">
            Đăng kí để không bỏ lỡ cơ hội nào !
          </p>
        </div>
      </div>
    </SectionContainer>
  )
}
