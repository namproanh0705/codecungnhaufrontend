import { FC, memo, useCallback, useMemo, useState } from 'react'
import { CourseDetailGuide } from 'services/api-course-detail-guide.type'
import CourseBanner, { IBannerProps } from './CourseBanner'
import { ITabProps } from './CourseTab'
import GuideTab from './GuideTab'

interface IProps {
  slug: string | string[]
}

const CourseDetail: FC<IProps> = ({ slug }) => {
  const [tab, setTab] = useState('guide')

  const _: CourseDetailGuide[] = useMemo(
    () => [
      {
        title: 'Tiêu đề chương 1',
        alt: '7 bài học',
        child: [
          { title: 'Tiêu đề bài 1', href: '' },
          { title: 'Tiêu đề bài 2', href: '' },
          { title: 'Tiêu đề bài 3', href: '' },
          { title: 'Tiêu đề bài 4', href: '' },
          { title: 'Tiêu đề bài 5', href: '' },
          { title: 'Tiêu đề bài 6', href: '' },
          { title: 'Tiêu đề bài 7', href: '' },
        ],
      },
      {
        title: 'Tiêu đề chương 2',
        alt: '2 bài học',
        child: [
          { title: 'Tiêu đề bài 1', href: '' },
          { title: 'Tiêu đề bài 2', href: '' },
        ],
      },
      {
        title: 'Tiêu đề chương 3',
        alt: '2 bài học',
        child: [
          { title: 'Tiêu đề bài 1', href: '' },
          { title: 'Tiêu đề bài 2', href: '' },
        ],
      },
    ],
    []
  )

  const tabData: ITabProps[] = useMemo(
    () => [
      {
        title: 'HƯỚNG DẪN',
        type: 'guide',
      },
      {
        title: 'VÍ DỤ',
        type: 'example',
      },
      {
        title: 'LIÊN QUAN',
        type: 'relative',
      },
    ],
    []
  )

  const onTabChange = useCallback<IBannerProps['onTabChange']>((type) => {
    setTab(type)
  }, [])

  return (
    <>
      <CourseBanner
        title={'Học lập trình Java'}
        icon={'https://www.pngall.com/wp-content/uploads/2016/05/Java-Free-Download-PNG.png'}
        description={
          'Java is a powerful general-purpose programming language. It is used to develop desktop and mobile applications, big data processing, embedded systems, and so on. According to Oracle, the company that owns Java, Java runs on 3 billion devices worldwide, which makes Java one of the most popular programming languages.\n\nOur Java tutorial will guide you to learn Java one step at a time.'
        }
        activeTab={tab}
        tabData={tabData}
        onTabChange={onTabChange}
      />
      <GuideTab data={_} />
    </>
  )
}

export default memo(CourseDetail)
