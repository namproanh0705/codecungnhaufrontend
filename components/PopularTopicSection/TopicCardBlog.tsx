import Card from '@/components/Card'
import classNames from '@/lib/utils/classNames'
import TopDecoSvg from '@/resources/svg/cardDecoration/cardDeco-1-wide.svg'
import CategoryJavaSvg from '@/resources/svg/categoryIcon/java.svg'
import CategoryPythonSvg from '@/resources/svg/categoryIcon/python.svg'
import CategoryQuestionSvg from '@/resources/svg/categoryIcon/question.svg'
import { FC, memo } from 'react'
import Image from '../Image'

const CategorySvg = {
  java: CategoryJavaSvg,
  python: CategoryPythonSvg,
  question: CategoryQuestionSvg,
}

type TIconType = keyof typeof CategorySvg

type TBlogData = {
  author: { name: string; src: string; popular: string }
  title: string
  type: TIconType
  heart: number
  comment: number
  date: string
}

interface IProps {
  data: TBlogData
  className?: string
}

const TopicCardBlog: FC<IProps> = ({
  data: {
    author: { name, src, popular },
    title,
    type,
    heart,
    comment,
    date,
  },
  className,
}) => {
  const Svg = CategorySvg[type] ?? CategorySvg.question
  return (
    <Card className={classNames('relative border border-gray-1', className)}>
      <div className="pointer-events-none absolute top-0 left-0 right-0 overflow-hidden">
        <TopDecoSvg />
      </div>
      <Svg className="mx-auto mt-4 aspect-square w-10 fill-primary" />

      <div className="mx-4 mt-2 flex h-10">
        <div className="relative aspect-square h-full overflow-hidden rounded-full">
          <Image src={src} alt="User avatar" layout="fill" draggable={false} />
        </div>

        <div className="ml-3 flex h-full flex-1 flex-col items-start justify-between overflow-hidden">
          <p className="w-full truncate font-semibold text-dark-2">{name}</p>
          <p className="w-full truncate text-xs text-dark-1">{popular}</p>
        </div>
      </div>

      <p className="mx-8 mt-3 min-h-[3rem] text-dark-2 line-clamp-2">{title}</p>

      <p className="mx-4 my-3 text-xs text-gray-4">{date}</p>

      <ul className="flex h-8 divide-x divide-gray-1 border-t border-gray-1">
        <li className="flex flex-1 items-center justify-center gap-2 font-light text-gray-3">
          <div>
            <div className="ti-heart h-3 text-xs" />
          </div>
          <p className="text-sm">{heart}</p>
        </li>
        <li className="flex flex-1 items-center justify-center gap-2 font-light text-gray-3">
          <div>
            <div className="ti-comment h-3 text-xs" />
          </div>
          <p className="text-sm">{comment}</p>
        </li>
        <button className="flex flex-1 items-center justify-center gap-2 font-light text-gray-3">
          <div>
            <div className="ti-bookmark h-3 text-xs" />
          </div>
        </button>
      </ul>
    </Card>
  )
}

export default memo(TopicCardBlog)

export type { TIconType, TBlogData }
