import Link from './Link'
import siteMetadata from '@/data/siteMetadata'
import SocialIcon from '@/components/social-icons'
// import projectsData from '@/data/projectsData'
import Logofooter from '@/data/logofooter.svg'
import footerData from '@/data/FooterData'

export default function Footer() {
  return (
    <footer>
      <div className="flex flex-wrap justify-between bg-[#051A4D] py-4 px-7 text-white dark:bg-zinc-800">
        <div>
          <Logofooter />
          <p className="pt-4 text-sm">{footerData[0].logoDetail}</p>
          <div className="mb-2 flex space-x-2 py-3 text-sm text-gray-500 dark:text-gray-400">
            <div>{siteMetadata.author}</div>
            <div>{` • `}</div>
            <div>{`© ${new Date().getFullYear()}`}</div>
          </div>
          <div className="mb-3 flex justify-center space-x-4">
            {footerData[1].tagIcon.map((d, i) => {
              // eslint-disable-next-line react/jsx-key
              return <SocialIcon key={i} kind={d.kind} href={d.href} size={d.size} />
            })}
          </div>
        </div>
        {footerData[2].tag.map((d, i) => {
          return (
            // eslint-disable-next-line react/jsx-key
            <div key={i}>
              <Link href={d.linkTag}>
                <h1 className="py-6 text-lg">{d.tagName}</h1>
              </Link>
              {d.detailTag.map((e, i) => {
                return (
                  // eslint-disable-next-line react/jsx-key
                  <Link href={e.link} key={i}>
                    <p className="pb-1 text-sm">{e.name}</p>
                  </Link>
                )
              })}
            </div>
          )
        })}
      </div>
    </footer>
  )
}
