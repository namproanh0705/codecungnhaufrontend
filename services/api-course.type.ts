export type TCourseList = {
  count: string
  next: number
  previous: number
  results: TCourseListData[]
}

export type TCourseListData = {
  slug: string
  logo: string
  name: string
  description: string
}
