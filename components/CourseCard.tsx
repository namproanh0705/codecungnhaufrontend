import classNames from '@/lib/utils/classNames'
import { FC, memo } from 'react'
import { DefaultProp } from 'types/DefaultProp'
import Button from './Button'
import Card from './Card'
import Image from './Image'

interface IProps extends DefaultProp {
  logo?: string
  name?: string
  slug?: string
  more?: boolean
}

const PATH = '/learn/'

const CourseCard: FC<IProps> = ({ logo, name, slug, more, className }) => {
  return (
    <Card
      className={classNames(
        'h-full shrink-0 border border-gray-1 duration-150 hover:border-primary',
        className
      )}
      style={{
        backgroundImage: `url(/static/images/bgHC3.svg)`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: '105%',
      }}
    >
      <Button isSimple isFull isWrap href={PATH + (!more ? slug : '')}>
        <div className="flex h-full flex-col items-start justify-center gap-9 px-4 py-8 text-left">
          {!more ? (
            <>
              <div>
                <Image alt={name} src={logo} width={40} height={40} unoptimized />
              </div>
              <p className="flex-1 p-2 text-2xl font-bold leading-10 text-dark-2 dark:text-white">
                Lập trình {name}
              </p>
              <div
                className="group flex w-fit items-center font-semibold text-primary"
                aria-label={`Link to ${slug}`}
              >
                <span>Xem ngay</span>
                <span className="inline-block pl-7 text-sm transition-transform duration-300 group-hover:translate-x-5">
                  <i className="ti-angle-right"></i>
                </span>
              </div>
            </>
          ) : (
            <>
              <div>
                <div className="group flex flex-1 items-center p-2 text-2xl font-semibold leading-10 text-primary dark:text-white">
                  <span>Xem tất cả</span>
                  <span className="inline-block pl-7 text-4xl transition-transform duration-300 group-hover:translate-x-5">
                    <i className="ti-angle-right"></i>
                  </span>
                </div>
              </div>
            </>
          )}
        </div>
      </Button>
    </Card>
  )
}

export default memo(CourseCard)
