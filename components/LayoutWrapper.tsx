import { ReactNode } from 'react'
import Footer from './Footer'
import Header from './Header'

interface Props {
  children: ReactNode
}

const LayoutWrapper = ({ children }: Props) => {
  return (
    <>
      <Header />
      <main className="mb-auto">{children}</main>
      <Footer />
    </>
  )
}

export default LayoutWrapper
