import FacebookIcon from '@/resources/svg/facebookIcon.svg'
import GoogleIcon from '@/resources/svg/googleIcon.svg'
import { FC, memo } from 'react'
import SocialButton from './SocialButton'

interface IProps {
  allowSocial: boolean
}

const SocialLogin: FC<IProps> = ({ allowSocial }) => {
  return (
    <div className="grid grid-cols-2 gap-2">
      <SocialButton
        alt="Google icon"
        label="Google"
        Svg={GoogleIcon}
        disabled={!allowSocial}
        // handleClick={() => {}}
      />
      <SocialButton
        alt="Facebook icon"
        label="Facebook"
        Svg={FacebookIcon}
        disabled={!allowSocial}
        // handleClick={() => {}}
      />
    </div>
  )
}

export default memo(SocialLogin)
