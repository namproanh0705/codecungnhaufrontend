import CourseDetail from '@/components/CourseDetail'
import { useRouter } from 'next/router'
import { memo } from 'react'

const Learn = () => {
  const router = useRouter()
  const { slug } = router.query // eslint-disable-line

  return (
    <>
      <CourseDetail slug={slug} />
    </>
  )
}

export default memo(Learn)
