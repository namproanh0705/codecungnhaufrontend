export interface CourseDetailGuide {
  title: string
  alt: string
  child: CourseDetailGuideChild[]
}

export interface CourseDetailGuideChild {
  title: string
  href: string
}
