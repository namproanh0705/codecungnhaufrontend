export type CategoryList = {
  count: string
  next: number
  previous: number
  results: CategoryChildren[]
}

export type CategoryChildren = {
  id: number
  name: string
  parent: number
  children: CategoryChildren[]
}
