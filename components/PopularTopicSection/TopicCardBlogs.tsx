// Incomplete - waiting for api to get filter prop works

import { FC, memo, useState } from 'react'
import Carousel from '../Carousel'
import TopicCardBlog, { TBlogData } from './TopicCardBlog'

interface IProps {
  filter?: string
}

const blogDataX: {
  id: string
  data: TBlogData
}[] = [
  {
    id: '21ldkadasjw',
    data: {
      author: {
        name: 'Nguyễn Tuyết',
        popular: '4 sao',
        src: 'https://i.pinimg.com/550x/a7/5b/aa/a75baa946a5b25a8f262b3cdf3c09934.jpg',
      },
      comment: 12,
      heart: 12,
      date: '4 ngày trước',
      title:
        'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam atque maxime corporis ea blanditiis, fuga perspiciatis possimus. Nemo est incidunt alias optio corporis esse aspernatur vitae quibusdam, maxime facere ex.',
      type: 'java',
    },
  },
  {
    id: '21lasddkajw',
    data: {
      author: {
        name: 'Nguyễn Tuyết',
        popular: '4 sao',
        src: 'https://i.pinimg.com/550x/a7/5b/aa/a75baa946a5b25a8f262b3cdf3c09934.jpg',
      },
      comment: 12,
      heart: 12,
      date: '4 ngày trước',
      title:
        'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam atque maxime corporis ea blanditiis, fuga perspiciatis possimus. Nemo est incidunt alias optio corporis esse aspernatur vitae quibusdam, maxime facere ex.',
      type: 'python',
    },
  },
  {
    id: '21ldxzkajw',
    data: {
      author: {
        name: 'Nguyễn Tuyết',
        popular: '4 sao',
        src: 'https://i.pinimg.com/550x/a7/5b/aa/a75baa946a5b25a8f262b3cdf3c09934.jpg',
      },
      comment: 12,
      heart: 12,
      date: '4 ngày trước',
      title:
        'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam atque maxime corporis ea blanditiis, fuga perspiciatis possimus. Nemo est incidunt alias optio corporis esse aspernatur vitae quibusdam, maxime facere ex.',
      type: 'question',
    },
  },
  {
    id: '21lcadkajw',
    data: {
      author: {
        name: 'Nguyễn Tuyết',
        popular: '4 sao',
        src: 'https://i.pinimg.com/550x/a7/5b/aa/a75baa946a5b25a8f262b3cdf3c09934.jpg',
      },
      comment: 12,
      heart: 12,
      date: '4 ngày trước',
      title:
        'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam atque maxime corporis ea blanditiis, fuga perspiciatis possimus. Nemo est incidunt alias optio corporis esse aspernatur vitae quibusdam, maxime facere ex.',
      type: 'java',
    },
  },
  {
    id: '21ldwakajw',
    data: {
      author: {
        name: 'Nguyễn Tuyết',
        popular: '4 sao',
        src: 'https://i.pinimg.com/550x/a7/5b/aa/a75baa946a5b25a8f262b3cdf3c09934.jpg',
      },
      comment: 12,
      heart: 12,
      date: '4 ngày trước',
      title:
        'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam atque maxime corporis ea blanditiis, fuga perspiciatis possimus. Nemo est incidunt alias optio corporis esse aspernatur vitae quibusdam, maxime facere ex.',
      type: 'java',
    },
  },
]

const TopicCardBlogs: FC<IProps> = () => {
  const [blogData, setBlogData] = useState(blogDataX) // eslint-disable-line

  // useEffect(() => {
  //   setTimeout(() => {
  //     setBlogData([
  //       {
  //         id: '21ldwadaskajw',
  //         data: {
  //           author: {
  //             name: 'Nguyễn Tuyết 2',
  //             popular: '5 sao',
  //             src: 'https://i.pinimg.com/550x/a7/5b/aa/a75baa946a5b25a8f262b3cdf3c09934.jpg',
  //           },
  //           comment: 12,
  //           heart: 132,
  //           date: '1 tuần trước',
  //           title:
  //             'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam atque maxime corporis ea blanditiis, fuga perspiciatis possimus. Nemo est incidunt alias optio corporis esse aspernatur vitae quibusdam, maxime facere ex.',
  //           type: 'python',
  //         },
  //       },
  //       ...blogData,
  //     ])
  //   }, 3000)
  // }, [])

  return (
    <div className="-mx-2">
      <Carousel className="w-full">
        {blogData.map(({ id, data }) => (
          <div key={id} className="w-1/4 flex-shrink-0 select-none px-2">
            <TopicCardBlog className="mb-9" data={data} />
          </div>
        ))}
      </Carousel>
    </div>
  )
}

export default memo(TopicCardBlogs)
