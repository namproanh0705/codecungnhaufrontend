import classNames from '@/lib/utils/classNames'
import anime from 'animejs'
import { FC, memo, ReactNode, useCallback, useEffect, useRef } from 'react'

interface IProps {
  className: string
  children?: ReactNode
}

const transitionDuration = 1000
const scrollThreshHold = 0.2 //should be from 0 - 1 (double type)

const Carousel: FC<IProps> = ({ className, children }) => {
  const lastX = useRef(0)
  const isDown = useRef(false)
  const startX = useRef(0)
  const lastMouseMoveX = useRef(0)
  const lastMouseMoveDiffX = useRef(0)
  const unameVar = useRef(0)

  const animeX = useRef<ReturnType<anime>>(null)

  const container = useRef<HTMLDivElement>(null)
  const containerScreenWidth = useRef(0)
  const containerWidth = useRef(0)
  const maxContainerWidth = useRef(0)
  const childWidth = useRef(0)

  const scrollEle = useRef<HTMLDivElement>(null)
  const scrollEleWidth = useRef(0)

  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  const updateScrollBar = useCallback(() => {
    scrollEle.current.style.marginLeft = `${(-lastX.current / containerWidth.current) * 100}%`
  }, [])

  const updateWidths = useCallback(() => {
    childWidth.current = (container.current.firstChild as HTMLElement)?.offsetWidth ?? 0

    containerScreenWidth.current = container.current.offsetWidth
    containerWidth.current = container.current.children.length * childWidth.current
    maxContainerWidth.current = containerWidth.current - containerScreenWidth.current

    scrollEleWidth.current = (containerScreenWidth.current / containerWidth.current) * 100
    scrollEle.current.style.width = `${scrollEleWidth.current}%`
  }, [])

  const updateLastX = useCallback(
    (x: number) => {
      lastX.current = x
      updateScrollBar()
    },
    [updateScrollBar]
  )

  const putThemInOrders = useCallback(
    (pixel: number, ms: number) => {
      animeX.current = anime({
        targets: container.current,
        translateX: pixel,
        easing: 'easeOutCubic',
        duration: ms,
        update: (_) => updateLastX(parseInt(_.animations[0].currentValue)),
      })
    },
    [updateLastX]
  )

  const getXInRange = useCallback((x: number) => {
    return x > 0 ? 0 : x < -maxContainerWidth.current ? -maxContainerWidth.current : x
  }, [])

  const reScroll = useCallback(() => {
    if (maxContainerWidth.current < 0) {
      putThemInOrders(0, transitionDuration)
      return
    }

    const newLastX = getXInRange(
      lastX.current - lastMouseMoveDiffX.current * scrollThreshHold * 100
    )

    const duration =
      newLastX === 0 || newLastX === -maxContainerWidth.current
        ? transitionDuration
        : Math.abs(newLastX - lastX.current) * 2

    // Cuộn về lề trái/phải nếu elements lồi ra ngoài
    newLastX !== lastX.current &&
      putThemInOrders(
        newLastX,
        newLastX === 0 || newLastX === -maxContainerWidth.current ? transitionDuration : duration
      )
  }, [getXInRange, putThemInOrders])
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  const handleMouseDown: EventListener = useCallback((e: MouseEvent & TouchEvent) => {
    anime.remove(container.current)

    isDown.current = true
    startX.current = e.type === 'mousedown' ? e.pageX : e.touches[0].pageX

    lastMouseMoveX.current = startX.current
    unameVar.current = lastMouseMoveX.current
  }, [])

  const handleMouseMove: EventListener = useCallback(
    (e: MouseEvent & TouchEvent) => {
      if (!isDown.current) return

      const eX = e.type === 'mousemove' ? e.pageX : e.touches[0].pageX

      lastMouseMoveDiffX.current = lastMouseMoveX.current - eX

      updateLastX(lastX.current - (unameVar.current - eX))
      unameVar.current = eX

      container.current.style.transform = `translateX(${lastX.current}px)`

      lastMouseMoveX.current = eX
    },
    [updateLastX]
  )

  const handleMouseUp: EventListener = useCallback(() => {
    if (!isDown.current) return
    isDown.current = false

    reScroll()
  }, [reScroll])
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  useEffect(() => {
    updateWidths()
    updateScrollBar()
  })

  // This function run ONE TIME only
  useEffect(() => {
    const containerEle = container.current

    containerEle?.addEventListener('mousedown', handleMouseDown)
    containerEle?.addEventListener('touchstart', handleMouseDown)
    document.addEventListener('mousemove', handleMouseMove)
    document.addEventListener('touchmove', handleMouseMove)
    document.addEventListener('mouseup', handleMouseUp)
    document.addEventListener('touchend', handleMouseUp)
    window.addEventListener('resize', updateWidths)

    return () => {
      containerEle?.removeEventListener('mousedown', handleMouseDown)
      containerEle?.removeEventListener('touchstart', handleMouseDown)
      document.removeEventListener('mousemove', handleMouseMove)
      document.removeEventListener('touchmove', handleMouseMove)
      document.removeEventListener('mouseup', handleMouseUp)
      document.removeEventListener('touchend', handleMouseUp)
      window.removeEventListener('resize', updateWidths)
    }
  }, []) // eslint-disable-line
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  return (
    <div className="relative flex flex-col items-center overflow-hidden">
      <div className={classNames('flex will-change-transform', className)} ref={container}>
        {children}
      </div>
      {/* simulate scroll bar */}
      <div className="h-1 w-full max-w-xs overflow-hidden rounded-full bg-gray-1">
        <div
          className="h-full rounded-full bg-primary duration-300"
          style={{
            marginLeft: `0%`,
            width: `${scrollEleWidth.current}%`,
            transitionProperty: `width`,
          }}
          ref={scrollEle}
        ></div>
      </div>
    </div>
  )
}

export default memo(Carousel)
