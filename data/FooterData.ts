const footerData = [
  {
    logoDetail: 'Code Cùng Nhau - cùng nhau chia sẻ kiến thức lập trình',
  },
  {
    tagIcon: [
      { kind: `youtube`, href: 'https://www.youtube.com/', size: 6 },
      { kind: 'facebook', href: 'https://www.facebook.com/', size: 6 },
      { kind: 'facebook', href: 'https://www.facebook.com/', size: 8 },
      { kind: 'facebook', href: 'https://www.facebook.com/', size: 10 },
    ],
  },
  {
    tag: [
      {
        tagName: 'Về Code Cùng nhau',
        linkTag: '/about',
        detailTag: [
          { name: 'Giới thiệu', link: '/' },
          { name: 'Dịch vụ', link: '/' },
          { name: 'Liên hệ', link: '/' },
        ],
      },
      {
        tagName: 'Học tập',
        linkTag: '/learn',
        detailTag: [
          { name: 'C', link: '/' },
          { name: 'C++', link: '/' },
        ],
      },
      {
        tagName: 'Blog',
        linkTag: '/blog',
        detailTag: [
          { name: 'Chủ đề IoT', link: '/' },
          { name: 'Chủ đề Web', link: '/' },
        ],
      },
    ],
  },
]
export default footerData
