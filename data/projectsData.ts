const projectsData = [
  {
    title: `C`,
    description: `C là một ngôn ngữ lập trình đa năng mạnh mẽ, được sử dụng để phát triển phần mềm như hệ điều hành, cơ sở dữ liệu, trình biên dịch,...`,
    imgSrc: '/static/images/bgHC.svg',
    href: '/blog',
  },
  {
    title: 'C++',
    description: `C ++ là 1 ngôn ngữ lập trình hướng đối tượng được phát triển dựa trên nền tảng của ngôn ngữ C, được ứng dụng trong các phần mềm, hệ điều hành và cả trình biên dịch.`,
    imgSrc: '/static/images/bgHC.svg',
    href: '/blog',
  },
  {
    title: 'Python',
    description: `Python là 1 ngôn ngữ lập trình bậc cao, cú pháp thân thiện, gõ không lo lỗi thiếu dấu ";" và làm được gần như mọi thứ, nổi bật bất là AI.`,
    imgSrc: '/static/images/bgHC.svg',
    href: '/blog',
  },
]

export default projectsData
