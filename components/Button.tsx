import classNames from '@/lib/utils/classNames'
import { ButtonHTMLAttributes, forwardRef, ForwardRefRenderFunction, memo, ReactNode } from 'react'
import CustomLink from './Link'

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children?: ReactNode
  href?: string
  isSimple?: boolean
  isNoPadding?: boolean
  isClickText?: boolean
  isLoading?: boolean
  isFull?: boolean
  isHFull?: boolean
  isWFull?: boolean
  isWrap?: boolean
  isNoColor?: boolean
  handleClick?: () => void
}

const Button: ForwardRefRenderFunction<HTMLButtonElement, IProps> = (
  {
    isWFull,
    isHFull,
    isFull,
    isLoading,
    isNoPadding,
    isSimple,
    isClickText,
    isWrap,
    isNoColor,
    className,
    type,
    href,
    children,
    disabled,
    handleClick,
    ...rest
  },
  ref
) => {
  // Currently working on isLoading effect
  isFull && (isWFull = isHFull = true)
  isSimple && ((isNoColor = false), (isNoPadding = true))

  const classes = classNames(
    'appearance-none',
    href && (isWFull || isHFull) && 'flex items-center',
    isSimple ?? 'overflow-hidden rounded-lg',
    isLoading && !isSimple && 'overflow-hidden',
    isNoColor ?? 'bg-primary text-gray-1',
    isWrap ?? 'max-w-full truncate',
    isNoPadding ?? 'px-6',
    isClickText && 'text-primary',
    isHFull && 'h-full',
    isWFull && 'w-full',
    className
  )
  // isLoading && (disabled = true)

  return (
    <>
      {href ? (
        <CustomLink className={classes} href={href}>
          {children}
        </CustomLink>
      ) : (
        <button
          className={classes}
          disabled={disabled}
          type={type ?? 'button'}
          onClick={handleClick}
          {...rest}
          ref={ref}
        >
          {isLoading === true ? (
            <div className="relative flex h-full -translate-y-full items-center justify-center transition-transform duration-500">
              <span className="absolute flex h-full items-center justify-center">{children}</span>
              <span className="absolute top-full flex h-full items-center justify-center">
                <div className="aspect-square h-4/6 animate-spin rounded-full border-2 border-b-transparent bg-transparent"></div>
              </span>
            </div>
          ) : isLoading === false ? (
            <div className="relative flex h-full items-center justify-center transition-transform duration-500 ease-out">
              <span className="absolute flex h-full items-center justify-center">{children}</span>
              <span className="absolute top-full flex h-full items-center justify-center">
                <div className="aspect-square h-4/6 rounded-full border-2 border-b-transparent bg-transparent"></div>
              </span>
            </div>
          ) : (
            // This is when this component won't have loading effect
            <>{children}</>
          )}
        </button>
      )}
    </>

    // <button
    //   className={classes}
    //   disabled={disabled}
    //   type={type ?? 'button'}
    //   onClick={handleClick}
    //   {...rest}
    //   ref={ref}
    // >
    //   {href ? (
    //     <CustomLink
    //       className={classNames(
    //         'h-full w-full',
    //         (isWFull || isHFull) && 'flex items-center',
    //         isWrap ?? 'truncate'
    //       )}
    //       href={href}
    //     >
    //       {children}
    //     </CustomLink>
    //   ) : // If no href, means isLoading might be possible
    //   isLoading === true ? (
    //     <div className="relative flex h-full -translate-y-full items-center justify-center transition-transform duration-500">
    //       <span className="absolute flex h-full items-center justify-center">{children}</span>
    //       <span className="absolute top-full flex h-full items-center justify-center">
    //         <div className="aspect-square h-4/6 animate-spin rounded-full border-2 border-b-transparent bg-transparent"></div>
    //       </span>
    //     </div>
    //   ) : isLoading === false ? (
    //     <div className="relative flex h-full items-center justify-center transition-transform duration-500 ease-out">
    //       <span className="absolute flex h-full items-center justify-center">{children}</span>
    //       <span className="absolute top-full flex h-full items-center justify-center">
    //         <div className="aspect-square h-4/6 rounded-full border-2 border-b-transparent bg-transparent"></div>
    //       </span>
    //     </div>
    //   ) : (
    //     // This is when this component won't have loading effect
    //     <>{children}</>
    //   )}
    // </button>
  )
}

export default memo(forwardRef(Button))
