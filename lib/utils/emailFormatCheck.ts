const emailFormatCheck = (mail: string) => {
  // !!! This regex will cause the app freeze if the mail string is long
  // const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  // if (mail.match(mailFormat)) return true
  // return false

  //eslint-disable-next-line
  const mailFormat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/
  if (mail.match(mailFormat)) return true
  return false

  // const postA = mail.indexOf('@')
  // return postA > 0 && mail.indexOf('.', postA) > postA
}

export default emailFormatCheck
