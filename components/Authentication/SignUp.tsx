import Logo from '@/resources/svg/logoColoredText.svg'
import { FC, memo, useCallback } from 'react'
import Button from '../Button'
import AuthCard from './AuthCard'
import SignUpFormControl from './SignUpFormControl'

const SignUp: FC = () => {
  // const [allowSocial, setAllowSocial] = useState(true)

  const handleSubmit = useCallback(() => {
    // setAllowSocial(false)
  }, [])
  const handleError = useCallback(() => {
    // setAllowSocial(true)
  }, [])

  return (
    <AuthCard>
      <Logo className="mx-auto my-4 w-24" />
      <div className="mx-4">
        <SignUpFormControl handleError={handleError} handleSubmit={handleSubmit} />
      </div>
      <div className="mx-4 my-4 mb-9">
        <p className="mt-6 px-3 text-xs text-gray-4">
          Đã có tài khoản?
          <Button id="sign-in-submit-btn" className="ml-1" isNoPadding isSimple isClickText>
            Đăng nhập
          </Button>
        </p>
      </div>
    </AuthCard>
  )
}

export default memo(SignUp)
