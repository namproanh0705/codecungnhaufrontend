import classNames from '@/lib/utils/classNames'
import { FC, memo, useCallback, useState } from 'react'

type TOption = {
  code: string
  value: string
}

type TOptions = TOption[]

type THandleChange = (code: TOption['code']) => void

interface IProps {
  options: TOptions
  initSelected?: string
  className?: string
  handleChange?: THandleChange
}

const Filter: FC<IProps> = ({ className, options, initSelected, handleChange }) => {
  const [selected, setSelected] = useState<string>(initSelected ?? (options[0].code || ''))

  const handleClick = useCallback(
    (code: string) => {
      setSelected(code)
      handleChange && handleChange(code)
    },
    [handleChange]
  )

  return (
    <div className={classNames('overflow-hidden rounded-lg bg-gray-1 p-[1px]', className)}>
      <div className="flex justify-between text-xs">
        {options.map(({ code, value }) => (
          <button
            key={code}
            className={classNames(
              'rounded-lg px-2 py-0.5 transition duration-300',
              selected === code ? 'bg-white font-medium text-dark-2' : 'text-gray-4'
            )}
            data-code={code}
            onClick={(e) => handleClick(e.currentTarget.getAttribute('data-code'))}
          >
            {value}
          </button>
        ))}
      </div>
    </div>
  )
}

export default memo(Filter)

export type { TOptions, TOption, THandleChange }
