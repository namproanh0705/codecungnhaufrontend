import { PageSEO } from '@/components/SEO'
import siteMetadata from '@/data/siteMetadata'
import HomeCourse from '@/components/HomeCourse'
import Section1 from '@/components/comments/Section1'
import Section2 from '@/components/comments/Section2'
import { Subscribe } from '@/components/comments/Subscribe'
import PopularTopicSection from '@/components/PopularTopicSection'

export default function Home() {
  return (
    <>
      <PageSEO title={siteMetadata.title} description={siteMetadata.description} />
      <Section1 />
      <HomeCourse />
      <Section2 />
      <PopularTopicSection />
      <Subscribe />
    </>
  )
}
