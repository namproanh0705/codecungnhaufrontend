import { FC, memo } from 'react'
import CourseList from './CourseList'
import SectionContainer from './SectionContainer'

const LIMIT = 4

const HomeCourse: FC = () => {
  return (
    <SectionContainer>
      <CourseList limit={LIMIT} />
    </SectionContainer>
  )
}

export default memo(HomeCourse)
